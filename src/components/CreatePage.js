import React from 'react';
import { withRouter } from 'react-router-dom';
import { graphql } from 'react-apollo';
import Modal from 'react-modal';
import modalStyle from '../constants/modalStyle';
import gql from 'graphql-tag';

class CreatePage extends React.Component {
	state = {
		description: ''
	};

	render() {
		return (
			<Modal isOpen contentLabel="Create Post" style={modalStyle} onRequestClose={this.props.history.goBack}>
				<div className="pa4 flex justify-center bg-white">
					<div style={{ maxWidth: 400 }} className="">
						<input
							className="w-100 pa3 mv2"
							value={this.state.description}
							placeholder="Description"
							onChange={e => this.setState({ description: e.target.value })}
						/>
						{this.state.description && (
							<button className="pa3 bg-black-10 bn dim ttu pointer" onClick={this.handlePost}>
								Post
							</button>
						)}
					</div>
				</div>
			</Modal>
		);
	}

	handlePost = async () => {
		const { description } = this.state;
		await this.props.createPostMutation({ variables: { description } });
		this.props.history.replace('/');
	};
}

const CREATE_POST_MUTATION = gql`
	mutation CreatePostMutation($description: String!) {
		createPost(description: $description) {
			id
			description
		}
	}
`;

const CreatePageWithMutation = graphql(CREATE_POST_MUTATION, { name: 'createPostMutation' })(CreatePage);
export default withRouter(CreatePageWithMutation);
